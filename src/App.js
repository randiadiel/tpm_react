import React, { Component } from 'react';
import Home from './Home.js';
import './app.css';
import About from './about.js';

class App extends Component {
  state = { 
    status : 0
   }

changeState=(id)=>{
  this.setState({status: id});
}

  showHeader = () => {
    if(this.state.status === 0){
      return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="btn navbar-brand" onClick={()=>this.changeState(0)}>Randi Ltd.</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <a className="btn nav-item nav-link active" >Home <span className="sr-only">(current)</span></a>
          <a className="btn nav-item nav-link" onClick={()=>this.changeState(1)}>About Us</a>
          <a className="btn nav-item nav-link" onClick={()=>this.changeState(2)}>Contact Us</a>
          <a className="btn nav-item nav-link" onClick={()=>this.changeState(3)}>Our Service</a>
        </div>
      </div>
    </nav>
      );
    }
    else if(this.state.status === 1){
      return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="btn navbar-brand" onClick={()=>this.changeState(0)}>Randi Ltd.</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <a className="btn nav-item nav-link" onClick={()=>this.changeState(0)}>Home</a>
          <a className="btn nav-item nav-link active" >About Us <span className="sr-only">(current)</span></a>
          <a className="btn nav-item nav-link" onClick={()=>this.changeState(2)}>Contact Us</a>
          <a className="btn nav-item nav-link" onClick={()=>this.changeState(3)}>Our Service</a>
        </div>
      </div>
    </nav>
      );
    }
    else if(this.state.status === 2){
      return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="btn navbar-brand" onClick={()=>this.changeState(0)}>Randi Ltd.</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <a className="btn nav-item nav-link" onClick={()=>this.changeState(0)}>Home</a>
          <a className="btn nav-item nav-link" onClick={()=>this.changeState(1)}>About Us</a>
          <a className="btn nav-item nav-link active" >Contact Us <span className="sr-only">(current)</span></a>
          <a className="btn nav-item nav-link" onClick={()=>this.changeState(3)}>Our Service</a>
        </div>
      </div>
    </nav>
      );
    }
    else{ 
      return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="btn navbar-brand" onClick={()=>this.changeState(0)}>Randi Ltd.</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <a className="btn nav-item nav-link" onClick={()=>this.changeState(0)}>Home</a>
            <a className="btn nav-item nav-link" onClick={()=>this.changeState(1)}>About Us</a>
            <a className="btn nav-item nav-link" onClick={()=>this.changeState(2)}>Contact Us</a>
            <a className="btn nav-item nav-link active">Our Service <span className="sr-only">(current)</span></a>
          </div>
        </div>
      </nav>
      );
    }
  }

  showPage=()=>{
    if(this.state.status === 0){
      return <Home/>
    }
    else if (this.state.status === 1){
      return <About/>
    }
  }
  render() { 
    return ( <React.Fragment>
      {this.showHeader()}
      {this.showPage()}
    </React.Fragment> );
  }
}
 
export default App;